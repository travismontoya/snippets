{-# LANGUAGE NoMonomorphismRestriction #-}
{- sd.hs
   Finds the standard deviation of a given list
   Travis Montoya 
-}
module Main
        where

-- Return simple average
findMean :: Fractional a => [a] -> a
findMean x = sum x / fromIntegral (length x)

-- Return variance
findVariance :: Fractional a => [a] -> a
findVariance x =
               let v y = (y - findMean x) ^ 2
               in findMean $ map v x

-- Return standard deviation
findStdev :: Floating a => [a] -> a
findStdev x = sqrt $ findVariance x

-- Test case for findStdev
main = do
       let testlist = [1,2,3,4]
       return (findStdev testlist)
